extends Node3D

@onready var checkpoints : Array 
var current_checkpoint : Area3D
var num = 0
@export var points := 1

# Called when the node enters the scene tree for the first time.
func _ready():
	for _i in self.get_children():
		checkpoints.append(_i)
		checkpoints[checkpoints.size() - 1].visible = false
	CallCheckpoint()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
func _onAreaEntered(body : Node3D):
	print("Check")
	current_checkpoint.visible = false
	current_checkpoint.disconnect("body_entered",_onAreaEntered)
	CallCheckpoint()
	GameManager.mod_pun(points)
	
func CallCheckpoint():
	if(checkpoints.size()>num):
		current_checkpoint = checkpoints[num]
		current_checkpoint.visible = true
		current_checkpoint.connect("body_entered",_onAreaEntered)
		num +=1
	else:
		GameManager.game_over(true)

