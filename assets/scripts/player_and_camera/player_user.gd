extends CharacterBody3D


@export var SPEED := 5.0
@export var CONS_ANGULAR_SPEED : float = 0.5
@export var ANGULAR_SPEED : float = 0.7
@export var JUMP_VELOCITY := 4.5
@export var min_rotation := -1.5
@export var max_rotation := 1.5
signal movement_player(velocity : float, delta : float, direction : Vector3)

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var _snap_vector := Vector3.DOWN;

@onready var _spring_arm: Node3D = get_node("../../../CameraGimbal")

#func player_jump():
	## Handle Jump.
	#if is_on_floor():
		#velocity.y = JUMP_VELOCITY
		#_snap_vector = Vector3.ZERO
	#elif _snap_vector == Vector3.ZERO and is_on_floor():
		#_snap_vector = Vector3.DOWN

func _physics_process(delta):
	if not is_on_floor():
		velocity.y -= gravity * delta
func _process(delta):
	$"../..".rotation.x = clamp($"../..".rotation.x,min_rotation, max_rotation)
	$"..".rotation.z = clamp($"..".rotation.z,min_rotation,max_rotation)
		
func player_movement(delta : float, input_dir : Vector2):

	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	direction = direction.rotated(Vector3.UP,_spring_arm.rotation.y).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
		#rotation.x = direction.x * ANGULAR_SPEED * -1
		#rotation.z = direction.z * ANGULAR_SPEED * -1
		var rotation_direction = Vector3(velocity.z,0,velocity.x)
		rotate_player(rotation_direction,delta,CONS_ANGULAR_SPEED)
		
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)	
	move_and_slide()
	
func rotate_player(rotation_direction : Vector3, delta, angular_speed):
	$"../..".rotate_x(rotation_direction.normalized().x * angular_speed * delta * -1)
	$"..".rotate_z(rotation_direction.normalized().z * angular_speed * delta)
	
# Replace with function body.

func _on_player_input_movement_signal(delta, input_dir):
	player_movement(delta,input_dir)

	 # Replace with function body.


#func _on_player_input_jump_signal():
	#player_jump() # Replace with function body.

func _on_movement_player(velocity, delta, direction):
	#rotation = velocity/$MeshInstance3D.mesh.radius # Replace with function body.
	pass


func _on_player_input_rotation_signal(delta, input_rot):
	var rotation = (transform.basis * Vector3(input_rot.x, 0, input_rot.y)).normalized()
	rotation = rotation.rotated(Vector3.UP,_spring_arm.rotation.y).normalized()
	var rotation_direction = Vector3(rotation.z * -1, 0, rotation.x * -1)
	rotate_player(rotation_direction,delta,ANGULAR_SPEED) # Replace with function body.


func _on_rigid_body_3d_body_entered(body):
	print("call game over")
