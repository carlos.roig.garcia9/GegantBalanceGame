extends Node3D

var rotation_speed := PI*0.5
var invert_y = false
var invert_x = false
var mouse_control = true
var mouse_sensitivity := 0.005
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func get_input_keyboard(delta):
	var y_rotation = Input.get_axis("cam_rotation_left","camera_rotation_right")
	rotate_object_local(Vector3.UP, y_rotation * rotation_speed * delta)
	var x_rotation = Input.get_axis("camera_rotation_up", "camera_rotation_down")
	x_rotation = -x_rotation if invert_y else x_rotation
	$InnerGimbal.rotate_object_local(Vector3.RIGHT, x_rotation * rotation_speed * delta)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _unhandled_input(event):
	if(mouse_control and event is InputEventMouseMotion):
		if(event.relative.x != 0):
			var dir = 1 if invert_x else -1
			rotate_object_local(Vector3.UP, dir * event.relative.x * mouse_sensitivity)
		if(event.relative.y != 0):
			var dir = 1 if invert_y else -1
			$InnerGimbal.rotate_object_local(Vector3.RIGHT, dir * event.relative.y * mouse_sensitivity)
func _process(delta):
	if (!mouse_control):
		get_input_keyboard(delta)
	$InnerGimbal.rotation.x = clamp($InnerGimbal.rotation.x, -1.4, 0.5)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED);


	
	
