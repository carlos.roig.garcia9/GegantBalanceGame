extends Node3D

signal movement_signal(delta : float, input_dir : Vector2)
signal rotation_signal(delta : float, input_rot : Vector3)
signal jump_signal
signal attack_signal

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
		var input_dir = Input.get_vector("movement_left", "movement_right", "movement_forward", "movement_backwards")
		movement_signal.emit(delta,input_dir)
		var input_rot = Input.get_vector("rotation_left","rotation_right","rotation_up","rotation_down")
		rotation_signal.emit(delta,input_rot)
		#if Input.is_action_just_pressed("jump"):
			#jump_signal.emit()
		
